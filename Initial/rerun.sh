#!/bin/bash

docker stop kalamastra
docker rmi -f kalamastra
docker build . -t kalamastra
docker run --name kalamastra -d kalamastra
docker exec -it kalamastra /bin/sh

# entra y construye la base de datos
psql
\i ddl.sql
\q
python3 datos.py