## To run a Concourse installation:

Install Docker

Install Docker-Compose

## To develop Pipelines:

Download FLY on the machine you're going to deploy pipelines from

### Login to your Concourse in order to manage it
fly -t noosbit login -c http://54.201.111.95:8080 -u kalamastra -p <pass>

### Deploy a new pipeline or update it
fly -t noosbit set-pipeline -c noosbit-frontend-deploy.yml -p noosbit.com

you can view your pipeline here: http://54.201.111.95:8080/teams/main/pipelines/noosbit.com

the pipeline is currently paused. to unpause, either:
  - run the unpause-pipeline command:
    fly -t noosbit unpause-pipeline -p noosbit.com
  - click play next to the pipeline in the web ui

`fly -t tutorial set-pipeline -c noosbit-frontend-deploy.yml -p noosbit.com --load-vars-from ~/.ssh/creds.yml`

