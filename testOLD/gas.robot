*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${webBrowser}    firefox
${gasolinePage}    https://www.efectifactura.com/

${ticketNumber}    id:ticketNumber
${ticketStation}    id:stationNumber
${ticketTime}    id:time
${boton}   name:searchTicket

${ticketNumero}   $nombre
${ticketHora}   013238
${EstacionNumero}   08

*** Test Cases ***
Abre web
    AbreElNavegador

Ingresa Datos
    IngresaNumero
    IngresaEstacion
    IngresaTiempo
    Press Key    ${ticketNumber}   \\13
    Sleep   10s

Cierra web
    CierraNavegador

*** Keywords ***
AbreElNavegador
    Open browser            ${gasolinePage}             ${webBrowser}

IngresaNumero
        input text  ${ticketNumber}    ${ticketNumero}

IngresaEstacion
        input text  ${ticketStation}    ${EstacionNumero}

IngresaTiempo
        input text  ${ticketTime}    ${ticketHora}

ClcikBoton
       Click Button   ${boton}


CierraNavegador
    Close Browser