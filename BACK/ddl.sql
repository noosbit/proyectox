/* 
Creacion de la base de datos
*/

CREATE TABLE usuario (
userID INT PRIMARY KEY,
nombre TEXT,
paterno TEXT,
materno TEXT,
correo TEXT,
rfc TEXT,
pass TEXT
);

INSERT into usuario values (01, 'Daniel','Verdugo','daniel@noosbit.com','j71','dann123');
INSERT into usuario values (02, 'Jesus','Rodriguez','Jesus@gmail.com','k24','passgen');

alter user postgres with password 'dann123';