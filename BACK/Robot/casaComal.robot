# Caso de uso para California
#pasan todas las pruebas

*** Settings ***
Library  SeleniumLibrary


*** Variables ***
${webBrowser}    firefox
${linePage}   https://alsea.interfactura.com/wwwroot?opc=CAC

${ticketRFC}    id:rfc
${ticketNumber}    id:ticket
${ticketTotal}    id:total


${userRFC}    XAXX010101000
${userNumero}   123456789012345678
${userTotal}   367

*** Test Cases ***
Abre web
    AbreElNavegador

Ingresa RFC
    Ingresa RFC

Ingresa Ticket
    Ingresa Ticket

Ingresa Total
    Ingresa Total

Cierra web
    CierraNavegador

*** Keywords ***
AbreElNavegador
    Open browser    ${linePage}    ${webBrowser}
    Sleep    10s

Ingresa RFC
        Click Element    xpath://*[@id="rfc"] 
        #input text  xpath://*[@id="rfc"]    ${userRFC}

Ingresa Ticket
        input text  ${ticketNumber}   ${userNumero}

Ingresa Total
        input text  ${ticketTotal}    ${userTotal}


CierraNavegador
    Close Browser