# Caso de uso para California
#pasan todas las pruebas

*** Settings ***
Library  SeleniumLibrary


*** Variables ***
${webBrowser}    firefox
${linePage}    https://alsea.interfactura.com/facturacioncpk/

${ticketRFC}    id:Main_RegistroClienteTicket11_txtRFC
${ticketNumber}    id:Main_RegistroClienteTicket11_txtTicket
${ticketTotal}    id:Main_RegistroClienteTicket11_txtTotal


${userRFC}    XAXX010101000
${userNuemro}   123456789012345678
${userTotal}   367

*** Test Cases ***
Abre web
    AbreElNavegador

Ingresa RFC
    IngresaRFC

Ingresa Numero
    IngresaNumero

Ingresa Total
    IngresaTotal

Cierra web
    CierraNavegador

*** Keywords ***
AbreElNavegador
    Open browser    ${linePage}    ${webBrowser}

IngresaRFC
        input text  ${ticketRFC}    ${userRFC}

IngresaTotal
        input text  ${ticketTotal}    ${userTotal}

IngresaNumero
        input text  ${ticketNumber}    ${userNuemro}


CierraNavegador
    Close Browser