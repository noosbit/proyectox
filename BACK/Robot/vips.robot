# Caso de uso para California
#pasan todas las pruebas

*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${webBrowser}    firefox
${linePage}    https://alsea.interfactura.com/RegistroDocumento.aspx?opc=Vips

${ticketRFC}    id:rfc
${ticketNumber}    id:ticket
${ticketTotal}    id:total


${userRFC}    XAXX010101000
${userNumero}   123456789012345678
${userTotal}   367

*** Test Cases ***
Abre web
    AbreElNavegador

Ingresa RFC
    IngresaRFC

Ingresa Ticket
    Ingresa Ticket

Ingresa Total
    IngresaTotal

Cierra web
    CierraNavegador

*** Keywords ***
AbreElNavegador
    Open browser    ${linePage}    ${webBrowser}

Ingresa RFC
        input text  ${ticketRFC}    ${userRFC}

Ingresa Ticket
        input text  ${ticketNumber}   ${userNumero}

Ingresa Total
        input text  ${ticketTotal}    ${userTotal}

CierraNavegador
    Close Browser