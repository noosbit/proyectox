*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${webBrowser}    firefox
${gasolinePage}    https://facturacion.walmartmexico.com.mx/frmDatos.aspx

${rfc}    id:ctl00_ContentPlaceHolder1_txtMemRFC
${buttonFacturar}   //*[@id="aspnetForm"]/div[3]/div[1]/div/div[2]/div[3]
${ticketNumber}    id:ctl00_ContentPlaceHolder1_txtTC
${codigoPostal}    id: ctl00_ContentPlaceHolder1_txtCP
${ticketStation}    id:ctl00_ContentPlaceHolder1_txtTR
${boton}   id:ctl00_ContentPlaceHolder1_btnAceptar

${clienteRFC}    XAXX010101000
${clienteNuemro}   102931297099287
${clienteCP}   01323
${clienteTrans}   400

*** Test Cases ***
Abre web
    AbreElNavegador

Click
    Click

Datos
    IngresaRFC
    IngresaCP
    IngresaTrans
    IngresaNumero
    Sleep    10s

Cierra web
    CierraNavegador

*** Keywords ***
AbreElNavegador
    Open browser            ${gasolinePage}             ${webBrowser}

Click
    click element    xpath://*[@id="aspnetForm"]/div[3]/div[1]/div/div[2]/div[3]

IngresaRFC
        input text  ${rfc}    ${clienteRFC}

IngresaCP
        input text  ${codigoPostal}    ${clienteCP}

IngresaTrans
        input text  ${ticketStation}    ${clienteTrans}

IngresaNumero
        input text  ${ticketNumber}    ${clienteNuemro}


CierraNavegador
    Close Browser