#Caso de uso para Star bucks
# pasan todas las pruebas y es identico a dominos

*** Settings ***
Library  SeleniumLibrary


*** Variables ***
#variables para entrar a la pagina
${webBrowser}    firefox
${linePage}    https://alsea.interfactura.com/RegistroDocumento.aspx?opc=Starbucks

#variables identificadores de la pagina
${ticketRFC}    id:rfc
${ticketNumber}    id:ticket
${ticketTienda}    id:tienda
${ticketFecha}    id:dtFecha

#valores del usuario
${userRFC}    XAXX010101000
${userNuemro}   123456789012345678
${userTienda}    12345
${userFecha}   08/04/2021


*** Test Cases ***
Abre web
    AbreElNavegador

Ingresa RFC
    IngresaRFC

Ingresa Ticket
    Ingresa Ticket

Ingresa Tienda
    Ingresa Tienda

Ingresa Fecha
    Ingresa Fecha

Cierra web
    CierraNavegador


*** Keywords ***
AbreElNavegador
    Open browser            ${linePage}             ${webBrowser}

IngresaRFC
        input text  ${ticketRFC}    ${userRFC}

Ingresa Ticket
        input text  ${ticketNumber}    ${userNuemro}

Ingresa Tienda
        input text  ${ticketTienda}    ${userTienda}

Ingresa Fecha
        input text  ${ticketFecha}    ${userFecha}

CierraNavegador
    Close Browser