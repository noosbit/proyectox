*** Settings ***
Documentation   Generando la factura de PandaExpress
| Library | Selenium2Library | run_on_failure=AppiumLibrary.CapturePageScreenshot

*** Variables ***
${webBrowser}               chrome
${buttonFacturar}       xpath=xpath: //*[contains(text(), "FACTURAR")]
${textBoxClienteRFC}        name=clienteRFC
${textBoxTicketNumero}      name=ticketNumero
${textBoxTicketImporte}     name=ticketImporte
${selectUsoCFDI}            name=usoID
${buttonContinuar}          name:continuar

${clienteRFC}           SALL910121DYA
${ticketNumero}         44105807303000
${ticketImporte}        154.00
${unidadID}             1
${usoID}                1

${DELAY}                1

*** Test Cases ***
Abre el navegador
    Abre el navegador y pagina de facturación de Decathlon
Entrar a facturar
    Click en boton Crear            ${buttonFacturar}
Ingresar datos del usuario
    Ingresa RFC del usuario         ${textBoxClienteRFC}        ${clienteRFC}
    Ingresa numero de ticket        ${textBoxTicketNumero}      ${ticketNumero}
    Ingresa importe del ticket      ${textBoxTicketImporte}     ${ticketImporte}
    Selecciona uso del CFDI         ${selectUsoCFDI}            ${usoID}
Generar factura
    Click boton continuar           ${buttonContinuar}

*** Keywords ***
Abre el navegador y pagina de facturación de Decathlon
    Open Browser            https://face-suite.com/decathlon/             ${webBrowser}
    Set Selenium Implicit Wait  4

Cierra todo
    Close Browser

Click en boton Crear
    [Arguments]                    ${buttonFacturar}
    Set Selenium Implicit Wait     4
    Select frame    frmHome
    Select frame    mainframe
    # Wait Until Element Is Enabled  ${buttonFacturar}
    # Page Should Contain Button     ${buttonFacturar}
    Click Button                   ${buttonFacturar}

Ingresa RFC del usuario
    [Arguments]             ${textBoxClienteRFC}        ${clienteRFC}
    Input text              ${textBoxClienteRFC}        ${clienteRFC}
    Set Selenium Speed      ${DELAY}

Ingresa numero de ticket
    [Arguments]             ${textBoxTicketNumero}      ${ticketNumero}
    Input text              ${textBoxTicketNumero}      ${ticketNumero}
    Set Selenium Speed      ${DELAY}

Ingresa importe del ticket
    [Arguments]             ${textBoxTicketImporte}     ${ticketImporte}
    Input text              ${textBoxTicketImporte}     ${ticketImporte}
    Set Selenium Speed      ${DELAY}

Selecciona uso del CFDI
    [Arguments]             ${selectUsoCFDI}            ${usoID}
    Select From List By Value     ${selectUsoCFDI}        G01
    Set Selenium Speed      ${DELAY}

Click boton continuar
    [Arguments]             ${buttonContinuar}
    Click Button           ${buttonContinuar}
    Set Selenium Speed      ${DELAY}


