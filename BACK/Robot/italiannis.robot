*** Settings ***
Library  SeleniumLibrary


*** Variables ***
${webBrowser}    firefox
${linePage}    https://alsea.interfactura.com/RegistroDocumento.aspx?opc=Italiannis

${ticketRFC}    id:rfc
${ticketNumber}    id:ticket
${ticketTotal}    id:total


${userRFC}    XAXX010101000
${userNuemro}   123456789012345678
${userTotal}   367

*** Test Cases ***
Abre web
    AbreElNavegador

Espera
    Sleep   10s

Ingresa RFC
    IngresaRFC

Ingresa Numero
    IngresaNumero

Ingresa Total
    IngresaTotal

    Sleep   10s

Cierra web
    CierraNavegador

*** Keywords ***
AbreElNavegador
    Open browser            ${linePage}             ${webBrowser}

IngresaRFC
        input text  xpath://*[@id="rfc"]    ${userRFC}

IngresaTotal
        input text  ${ticketTotal}    ${userTotal}

IngresaNumero
        input text  ${ticketNumber}    ${userNuemro}


CierraNavegador
    Close Browser